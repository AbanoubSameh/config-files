#!/bin/sh

ssh-add -l > /dev/null 2> /dev/null
if [ "$?" = 2 ]; then
    eval "$(ssh-agent -s)"
fi
