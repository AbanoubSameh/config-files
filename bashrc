
#                   ___          ___          ___          ___          ___
#     _____        /\  \        /\__\        /\  \        /\  \        /\__\
#    /::\  \      /::\  \      /:/ _/_       \:\  \      /::\  \      /:/  /
#   /:/\:\  \    /:/\:\  \    /:/ /\  \       \:\  \    /:/\:\__\    /:/  /
#  /:/ /::\__\  /:/ /::\  \  /:/ /::\  \  ___ /::\  \  /:/ /:/  /   /:/  /  ___
# /:/_/:/\:|__|/:/_/:/\:\__\/:/_/:/\:\__\/\  /:/\:\__\/:/_/:/__/___/:/__/  /\__\
# \:\/:/ /:/  /\:\/:/  \/__/\:\/:/ /:/  /\:\/:/  \/__/\:\/:::::/  /\:\  \ /:/  /
#  \::/_/:/  /  \::/__/      \::/ /:/  /  \::/__/      \::/~~/~~~~  \:\  /:/  /
#   \:\/:/  /    \:\  \       \/_/:/  /    \:\  \       \:\~~\       \:\/:/  /
#    \::/  /      \:\__\        /:/  /      \:\__\       \:\__\       \::/  /
#     \/__/        \/__/        \/__/        \/__/        \/__/        \/__/

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# general settings
HISTSIZE=-1
HISTFILESIZE=-1

set -o vi

# prevent mv and cp from overriding
alias mv='mv -i'
alias cp='cp -i'

# colors
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias pacman='pacman --color=auto'
alias sudo='sudo '

# single character aliases
alias s='sudo '
alias l='ls --color=auto'
alias c='clear'
alias p='python'
alias j='java'

# muli character aliases
alias py='python'
alias cl='clear'
alias jc='javac'
alias la='ls -A --color=auto'

# some colors defenitions
bold_blue="\[\e[1;34m\]"
white="\[\e[97m\]"
red="\[\e[31m\]"
reset_all="\[\e[0;0m\]"

# setting PS1
PS1="$bold_blue\u@\h $white\w$red\$$reset_all "

# clearing colors defenitions
unset bold_blue white red reset_all

