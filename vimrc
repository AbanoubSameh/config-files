
"                            ___          ___          ___
"      ___                  /\  \        /\  \        /\__\
"     /\  \      ___       |::\  \      /::\  \      /:/  /
"     \:\  \    /\__\      |:|:\  \    /:/\:\__\    /:/  /
"      \:\  \  /:/__/    __|:|\:\  \  /:/ /:/  /   /:/  /  ___
"  ___  \:\__\/::\  \   /::::|_\:\__\/:/_/:/__/___/:/__/  /\__\
" /\  \ |:|  |\/\:\  \__\:\~~\  \/__/\:\/:::::/  /\:\  \ /:/  /
" \:\  \|:|  | ~~\:\/\__\\:\  \       \::/~~/~~~~  \:\  /:/  /
"  \:\__|:|__|    \::/  / \:\  \       \:\~~\       \:\/:/  /
"   \::::/__/     /:/  /   \:\__\       \:\__\       \::/  /
"    ~~~~         \/__/     \/__/        \/__/        \/__/

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" other plugins
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'altercation/vim-colors-solarized'
Plugin 'valloric/youcompleteme'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'yggdroot/indentline'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'vim-latex/vim-latex.git'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" general settings
set autoread

set incsearch
set hlsearch
set nomodeline

" numbers and indentations
set number
set relativenumber
set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set scrolloff=999
set sidescrolloff=999

" encoding
set encoding=utf-8
set fileencoding=utf-8

" colors
set t_Co=256
syntax on
colorscheme darkblue

" general behavior
set wrap
set linebreak
set showmatch
set showcmd

" set command auto-complete
set wildmenu

" brackets
inoremap ( ()<LEFT>
inoremap [ []<LEFT>
inoremap { {}<LEFT>

inoremap () ()
inoremap [] []
inoremap {} {}

inoremap (<Space> (
inoremap [<Space> [
inoremap {<Space> {

inoremap {<CR> {<Esc>o}<Esc>O

" quotes
inoremap ' ''<Left>
inoremap " ""<Left>

inoremap '' ''
inoremap "" ""

inoremap '<Space> '
inoremap "<Space> "

" disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" add my own shortcuts
noremap danc :r!addcomment -c "$(adddate)" <Enter>
noremap danC :r!addcomment -C "$(adddate)" <Enter>
noremap dans :r!addcomment -s "$(adddate)" <Enter>
noremap danq :r!addcomment -q "$(adddate)" <Enter>
noremap dand :r!addcomment -d "$(adddate)" <Enter>
noremap danx :r!addcomment -x "$(adddate)" <Enter>
noremap danv :r!addcomment -v "$(adddate)" <Enter>
noremap danl :r!addcomment -l1 "$(adddate)" <Enter>
noremap dann :r!adddate <Enter>

noremap canc :r!addcomment -c "$(addcopyright)" <Enter>
noremap canC :r!addcomment -C "$(addcopyright)" <Enter>
noremap cans :r!addcomment -s "$(addcopyright)" <Enter>
noremap canq :r!addcomment -q "$(addcopyright)" <Enter>
noremap cand :r!addcomment -d "$(addcopyright)" <Enter>
noremap canx :r!addcomment -x "$(addcopyright)" <Enter>
noremap canv :r!addcomment -v "$(addcopyright)" <Enter>
noremap canl :r!addcomment -l1 "$(addcopyright)" <Enter>
noremap cann :r!addcopyright <Enter>

noremap fanc :r!addcomment -c "$(adddate)" <Enter>:r!addcomment -c "$(addcopyright)" <Enter>
noremap fanC :r!addcomment -C "$(adddate)" <Enter>:r!addcomment -C "$(addcopyright)" <Enter>
noremap fans :r!addcomment -s "$(adddate)" <Enter>:r!addcomment -s "$(addcopyright)" <Enter>
noremap fanq :r!addcomment -q "$(adddate)" <Enter>:r!addcomment -q "$(addcopyright)" <Enter>
noremap fand :r!addcomment -d "$(adddate)" <Enter>:r!addcomment -d "$(addcopyright)" <Enter>
noremap fanx :r!addcomment -x "$(adddate)" <Enter>:r!addcomment -x "$(addcopyright)" <Enter>
noremap fanv :r!addcomment -v "$(adddate)" <Enter>:r!addcomment -v "$(addcopyright)" <Enter>
noremap fanl :r!addcomment -l1 "$(adddate)" <Enter>:r!addcomment -l1 "$(addcopyright)" <Enter>
noremap fann :r!adddate <Enter>:r!addcopyright <Enter>
