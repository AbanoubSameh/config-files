set $mod Mod4

floating_modifier $mod

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# These are my own key bindings
bindsym $mod+Shift+x exec --no-startup-id dm-tool switch-to-greeter
bindsym $mod+Shift+p exec --no-startup-id sudo systemctl suspend
bindsym $mod+Shift+d exec --no-startup-id sudo shutdown
bindsym $mod+Shift+z exec --no-startup-id sudo shutdown -r
bindsym $mod+Shift+f exec --no-startup-id sudo shutdown -c
bindsym $mod+Shift+e exec --no-startup-id i3-msg exit
bindsym $mod+b exec --no-startup-id ~/.config/my_scripts/i3_launch brave $(~/.config/my_scripts/get_current_workspace)
bindsym $mod+m exec --no-startup-id pcmanfm
bindsym $mod+t exec --no-startup-id urxvt -e sh -c "lf"
bindsym $mod+c exec --no-startup-id mate-calculator
bindsym $mod+Shift+g exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym $mod+Shift+v exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym $mod+Shift+i exec --no-startup-id i3-msg gaps inner all plus 5
bindsym $mod+Shift+o exec --no-startup-id i3-msg gaps outer all plus 5
bindsym $mod+i exec --no-startup-id i3-msg gaps inner all minus 5
bindsym $mod+o exec --no-startup-id i3-msg gaps outer all minus 5
bindsym $mod+Shift+b exec --no-startup-id i3-msg bar mode toggle
bindsym $mod+Shift+n exec --no-startup-id i3-msg workspace $(( $(~/.config/my_scripts/get_current_workspace)+1 ))
bindsym $mod+n exec --no-startup-id i3-msg workspace next
bindsym $mod+p exec --no-startup-id i3-msg workspace previous

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym h resize shrink width 5 px or 5 ppt
        bindsym j resize grow height 5 px or 5 ppt
        bindsym k resize shrink height 5 px or 5 ppt
        bindsym l resize grow width 5 px or 5 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3blocks
        position top
        font pango:mono 9
        colors{
               background #232323
               statusline #DCDCDC
        }
}

# You can also use any non-zero value if you'd like to have a border
default_border pixel 1
default_floating_border pixel 50

gaps inner 5
gaps outer 5

exec --no-startup-id setxkbmap -option grp:switch,grp:alt_shift_toggle,grp_led:scroll us,ar
exec --no-startup-id light-locker &

set $MYLOCKTIME 5
set $MYWALLPAPERCHANGETIME 2

exec --no-startup-id ~/.config/my_scripts/lockerd $MYLOCKTIME &
exec --no-startup-id ~/.config/my_scripts/wall_changerd $MYWALLPAPERCHANGETIME &

exec i3-sensible-terminal
